from rest_framework.routers import DefaultRouter
from marketapi.views import CityView, StreetView, MarketView

router = DefaultRouter()
router.register('city', viewset=CityView)
router.register('street', viewset=StreetView, basename='street-city')
router.register('shop', viewset=MarketView, basename='shops')
urlpatterns = router.urls
