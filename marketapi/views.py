from rest_framework import permissions
from rest_framework import status
from rest_framework import viewsets
from rest_framework.authentication import BasicAuthentication, SessionAuthentication
from rest_framework.decorators import action, permission_classes
from rest_framework.response import Response

from marketapi.models import City, Street, Market
from marketapi.serializers import CitySerializer, StreetSerializer, MarketSerializer


# Create your views here.


class CityView(viewsets.ModelViewSet):
    authentication_classes = [BasicAuthentication, SessionAuthentication]
    queryset = City.objects.all()
    serializer_class = CitySerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    class Meta:
        model = City
        fields = '__all__'

    @action(detail=True, url_path='street', methods=['get'], url_name='Streets')
    def get_streets(self, request, pk=None):
        city = City.objects.get(pk=pk)
        street = city.street_set.all()
        serializer = StreetSerializer(street, many=True)
        try:
            return Response(serializer.data, status=status.HTTP_200_OK)
        except City.DoesNotExist:
            return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class StreetView(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAdminUser]
    queryset = Street.objects.all()
    serializer_class = StreetSerializer

    class Meta:
        model = Street
        fields = '__all__'


class MarketView(viewsets.ModelViewSet):
    queryset = Market.objects.all()
    serializer_class = MarketSerializer

    class Meta:
        model = Market
        fields = '__all__'

    @permission_classes(permission_classes=[permissions.IsAuthenticated])
    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data['id'], status=status.HTTP_200_OK, headers=headers)

    def perform_create(self, serializer):
        serializer.save()

    def get_queryset(self):
        from datetime import datetime
        from django.db.models import Q
        street = self.request.query_params.get(key='street', default=None)
        city = self.request.query_params.get(key='city', default=None)
        is_open = self.request.query_params.get(key='open', default=None)
        queryset = self.queryset
        if city is not None:
            queryset = queryset.filter(city__name=city)
        if street is not None:
            queryset = queryset.filter(street__name=street)
        if is_open is not None:
            current_time = datetime.now().time()
            if is_open == '1':
                queryset = queryset.filter(Q(time_open__lte=current_time) & Q(time_close__gt=current_time))
            elif is_open == '0':
                queryset = queryset.filter(Q(time_open__gt=current_time) | Q(time_close__lt=current_time))
        return queryset
