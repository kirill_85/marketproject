from django.db import models


# Create your models here.


class City(models.Model):
    name = models.CharField(max_length=200, blank=False, null=False)

    def __str__(self):
        return self.name


class Street(models.Model):
    name = models.CharField(max_length=255, null=False)
    city = models.ForeignKey('City', on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Market(models.Model):
    title = models.CharField(max_length=200, null=False, blank=False)
    city = models.ForeignKey('City', on_delete=models.CASCADE)
    street = models.ForeignKey('Street', on_delete=models.CASCADE)
    time_open = models.TimeField(null=False)
    time_close = models.TimeField(null=False)

    def __str__(self):
        return self.title
